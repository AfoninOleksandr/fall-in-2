#include "MainLevelScene.h"
#include "Services.h"
#include "EventHandler.h"
#include "SceneHandler.h"

#include "Character.h"
#include "CombatEncounter.h"
#include "CombatManager.h"

MainLevelScene::MainLevelScene(Services* servicesIn) : _services(servicesIn)
{
	AddSelfAsListener();
	Init();
}

MainLevelScene::~MainLevelScene()
{
	_services->GetEventHandler()->RemoveListener(_ptr);
}

void MainLevelScene::Init()
{
	
}

void MainLevelScene::AddSelfAsListener()
{
	// Groups to listen to
	_services->GetEventHandler()->AddListener(_ptr);
	_services->GetEventHandler()->AddLocalListener("MainLevel" ,_ptr);
}

void MainLevelScene::OnEvent(std::shared_ptr<const Event>& event)
{
	// If not active ignore events
	if (!_active)
		return;
}

void MainLevelScene::GetInputs()
{
	// Get keys pressed
	_keyEscapePressed = IsKeyPressed(KEY_ESCAPE);
}

void MainLevelScene::Enter()
{
	_active = true;

	// TEST 
	Texture2D testTex;
	MainStats mainStats = {5, 5, 5, 5, 5, 5, 5};
	Stats stats = CalculateStats(mainStats);
	Skills skills;
	skills.combatSkills.meleeWeapons = 75;
	std::vector<Perk> perks;
	std::vector<Trait> traits;

	Character character1("Char1", false, stats, skills, perks, traits, testTex, testTex);
	Character character2("Char2", true, stats, skills, perks, traits, testTex, testTex);

	Item knife;
	knife.name = "Knife";
	knife.type = MELEE;
	knife.itemInfo.damage = std::make_pair(4, 7);
	Equipment equipment = {character1.AddItem(knife)};
	character1.SetEquipment(equipment);

	std::vector<Character*> characters;
	characters.push_back(&character1);
	characters.push_back(&character2);

	CombatEncounter combatEncounter(_services, characters);

	std::unique_ptr<const Event> event = std::make_unique<const CombatEvent>(&character1, &character2);
	_services->GetEventHandler()->AddLocalEvent("CombatManager", std::move(event));
	// TEST
}

void MainLevelScene::Exit()
{
	_active = false;
}

void MainLevelScene::Update()
{
	if(!_active)
		return;

	GetInputs();

	// Exit scene and go to menu
	if (_keyEscapePressed && _active)
	{
		std::unique_ptr<const Event> event = std::make_unique<const NextSceneEvent>("Menu");
		_services->GetEventHandler()->AddLocalEvent("SceneHandler", std::move(event));

		_keyEscapePressed = false;
		_active = false;
		return;
	}
}

void MainLevelScene::Draw()
{
	if(!_active)
		return;
}