#include "CombatEncounter.h"
#include "Services.h"
#include "EventHandler.h"

#include "CombatEncounterUI.h"
#include "CombatManager.h"

CombatEncounter::CombatEncounter(Services* servicesIn, const std::vector<Character*>& charactersIn) : _services(servicesIn), _characters(charactersIn)
{
	AddSelfAsListener();
	Init();
}

CombatEncounter::~CombatEncounter()
{
	_services->GetEventHandler()->RemoveListener(_ptr);
}

void CombatEncounter::Init()
{
	_UI = std::make_unique<CombatEncounterUI>(_services);
	_combatManager = std::make_unique<CombatManager>(_services);
}

void CombatEncounter::AddSelfAsListener()
{
	_services->GetEventHandler()->AddListener(_ptr);
	_services->GetEventHandler()->AddLocalListener("CombatEncounter" ,_ptr);
}

void CombatEncounter::OnEvent(std::shared_ptr<const Event>& event)
{

}

void CombatEncounter::Update()
{
	_UI->Update();
}

void CombatEncounter::Draw()
{

}