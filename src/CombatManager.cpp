#include "CombatManager.h"
#include "Services.h"
#include "EventHandler.h"

CombatManager::CombatManager(Services* servicesIn) : _services(servicesIn)
{
	AddSelfAsListener();
	Init();
}

CombatManager::~CombatManager()
{
	_services->GetEventHandler()->RemoveListener(_ptr);
}

void CombatManager::Init()
{

}

void CombatManager::AddSelfAsListener()
{
	_services->GetEventHandler()->AddListener(_ptr);
	_services->GetEventHandler()->AddLocalListener("CombatManager" ,_ptr);
}

void CombatManager::OnEvent(std::shared_ptr<const Event>& event)
{
	if (std::shared_ptr<const CombatEvent> combatEvent = std::dynamic_pointer_cast<const CombatEvent>(event))
	{
		Log("Combat!");
		ResolveCombat(combatEvent->attacker, combatEvent->defender);
	}
}

void CombatManager::ResolveCombat(Character* attacker, Character* defender)
{
	Item* attackerWeapon = attacker->GetEquipment().mainWeapon.lock().get();
	std::string attackerWeaponName;

	int damage;

	// No weapon
	if(!attackerWeapon || (attackerWeapon->type != MELEE &&  attackerWeapon->type != GUN))
	{
		attackerWeaponName = "Unarmed";
		damage = ResolveUnarmed(attacker, defender);
	}

	// Attaker Melee
	else if (attackerWeapon->type == MELEE)
	{
		attackerWeaponName = attackerWeapon->name;
		damage = ResolveMelee(attacker, defender);
	}

	// Attacker Gun
	else if (attackerWeapon->type == GUN)
	{
		attackerWeaponName = attackerWeapon->name;
		damage = ResolveGun(attacker, defender);
	}

	// Hit and did damage TODO
	if (damage > 0)
	{
		Log(attacker->GetName() << " Attacked " << defender->GetName() << " with " << attackerWeaponName << " and did " << damage << " damage");
		return;
	}

	// No damage TODO
	if (damage = 0)
	{
		Log(attacker->GetName() << " Attacked " << defender->GetName() << " with " << attackerWeaponName << " and did no damage");
		return;
	}

	// Missed TODO
	if (damage = -1)
	{
		Log(attacker->GetName() << " Attacked " << defender->GetName() << " with " << attackerWeaponName << " and missed");
		return;
	}
}

int CombatManager::ResolveUnarmed(const Character* attacker, const Character* defender) const
{
	Stats attackerStats = attacker->GetStats();
	Skills attackerSkills = attacker->GetSkills();

	Item* defenderArmor = defender->GetEquipment().armor.lock().get();
	Stats defenderStats= defender->GetStats();
	Skills defenderSkills = defender->GetSkills();

	unsigned int hitChance;
	int possibleDamage;

	hitChance = attackerSkills.combatSkills.unarmed;
	possibleDamage = GetRandomValue(1, 2);

	if (attackerSkills.combatSkills.unarmed >= 55 && attackerStats.currentMainStats.agility >= 6)
	{
		possibleDamage += 3;
	}

	if (attackerSkills.combatSkills.unarmed >= 75 && attackerStats.currentMainStats.agility >= 6 && attackerStats.currentMainStats.strength >= 5 && attackerStats.variableStats.level >= 6)
	{
		possibleDamage += 5;
	}

	if (attackerSkills.combatSkills.unarmed >= 100 && attackerStats.currentMainStats.agility >= 7 && attackerStats.currentMainStats.strength >= 5 && attackerStats.variableStats.level >= 9)
	{
		possibleDamage += 7;
	}

	unsigned int random = GetRandomValue(1, 100);
	unsigned int requiredToHit = hitChance - defenderStats.calculatedStats.armorClass;

	if (random <= requiredToHit)
	{
		// If damage is less then threshold then no damage
		if (defenderArmor && possibleDamage - defenderArmor->itemInfo.damageThreshold <= 0)
		{
			return 0;
		}

		if (possibleDamage - defenderStats.calculatedStats.damageResist <= 0)
		{
			return 0;
		}

		return possibleDamage - defenderStats.calculatedStats.damageResist;
	}

	// We missed so -1 to indicate this
	else
	{
		return -1;
	}
}

int CombatManager::ResolveMelee(const Character* attacker, const Character* defender) const
{
	Item* attackerWeapon = attacker->GetEquipment().mainWeapon.lock().get();
	Stats attackerStats = attacker->GetStats();
	Skills attackerSkills = attacker->GetSkills();

	Item* defenderArmor = defender->GetEquipment().armor.lock().get();
	Stats defenderStats= defender->GetStats();
	Skills defenderSkills = defender->GetSkills();

	unsigned int hitChance;
	int possibleDamage;

	hitChance = attackerSkills.combatSkills.meleeWeapons;
	possibleDamage = GetRandomValue(attackerWeapon->itemInfo.damage.first, attackerWeapon->itemInfo.damage.second) + attackerStats.calculatedStats.meleeDamage;

	unsigned int random = GetRandomValue(1, 100);
	unsigned int requiredToHit = hitChance - defenderStats.calculatedStats.armorClass;

	if (random <= requiredToHit)
	{
		// If damage is less then threshold then no damage
		if (defenderArmor && possibleDamage - defenderArmor->itemInfo.damageThreshold <= 0)
		{
			return 0;
		}

		if (possibleDamage - defenderStats.calculatedStats.damageResist <= 0)
		{
			return 0;
		}

		return possibleDamage - defenderStats.calculatedStats.damageResist;
	}

	// We missed so -1 to indicate this
	else
	{
		return -1;
	}
}

int CombatManager::ResolveGun(const Character* attacker, const Character* defender) const
{
	Item* attackerWeapon = attacker->GetEquipment().mainWeapon.lock().get();
	Stats attackerStats = attacker->GetStats();
	Skills attackerSkills = attacker->GetSkills();

	Item* defenderArmor = defender->GetEquipment().armor.lock().get();
	Stats defenderStats= defender->GetStats();
	Skills defenderSkills = defender->GetSkills();

	unsigned int hitChance;

	if (attackerWeapon->itemInfo.gunType == SMALLGUN)
	{
		hitChance = attackerSkills.combatSkills.smallGuns;
	}

	if (attackerWeapon->itemInfo.gunType == BIGGUN)
	{
		hitChance = attackerSkills.combatSkills.bigGuns;
	}

	if (attackerWeapon->itemInfo.gunType == ENERGYWEAPON)
	{
		hitChance = attackerSkills.combatSkills.energyWeapons;
	}

	int possibleDamage = GetRandomValue(attackerWeapon->itemInfo.damage.first, attackerWeapon->itemInfo.damage.second);

	unsigned int random = GetRandomValue(1, 100);
	unsigned int requiredToHit = hitChance - defenderStats.calculatedStats.armorClass;

	if (random <= requiredToHit)
	{
		// If damage is less then threshold then no damage
		if (defenderArmor && possibleDamage - defenderArmor->itemInfo.damageThreshold <= 0)
		{
			return 0;
		}

		if (possibleDamage - defenderStats.calculatedStats.damageResist <= 0)
		{
			return 0;
		}

		return possibleDamage - defenderStats.calculatedStats.damageResist;
	}

	// We missed so -1 to indicate this
	else
	{
		return -1;
	}
}