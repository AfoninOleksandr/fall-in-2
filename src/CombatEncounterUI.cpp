#include "CombatEncounterUI.h"
#include "Services.h"
#include "EventHandler.h"

CombatEncounterUI::CombatEncounterUI(Services* servicesIn) : _services(servicesIn)
{
	AddSelfAsListener();
	Init();
}

CombatEncounterUI::~CombatEncounterUI()
{
	_services->GetEventHandler()->RemoveListener(_ptr);
}

void CombatEncounterUI::Init()
{

}

void CombatEncounterUI::AddSelfAsListener()
{
	_services->GetEventHandler()->AddListener(_ptr);
	_services->GetEventHandler()->AddLocalListener("CombatEncounterUI" ,_ptr);
}

void CombatEncounterUI::OnEvent(std::shared_ptr<const Event>& event)
{

}

void CombatEncounterUI::Update()
{

}

void CombatEncounterUI::Draw()
{

}