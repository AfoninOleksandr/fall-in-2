#include "Character.h"

Character::Character(const std::string& nameIn, const bool& enemyIn, const Stats& statsIn, const Skills& skillsIn, const std::vector<Perk> perksIn, const std::vector<Trait> traitsIn, const Texture2D& staticSpriteIn, const Texture2D& iconSpriteIn)
: _name(nameIn), _enemy(enemyIn), _stats(statsIn), _skills(skillsIn), _perks(perksIn), _traits(traitsIn), _staticSprite(staticSpriteIn), _iconSprite(iconSpriteIn)
{

}

Character::Character(const std::string& nameIn, const bool& enemyIn, const Stats& statsIn, const Skills& skillsIn, const std::vector<Perk> perksIn, const std::vector<Trait> traitsIn, const Texture2D& staticSpriteIn, const Texture2D& iconSpriteIn, const std::map<std::string, Animation>& animationsIn, const std::map<std::string, Sound>& soundsIn)
: _name(nameIn), _enemy(enemyIn), _stats(statsIn), _skills(skillsIn), _perks(perksIn), _traits(traitsIn), _staticSprite(staticSpriteIn), _iconSprite(iconSpriteIn), _animations(animationsIn), _sounds(soundsIn)
{

}

Character::~Character()
{
	
}

void Character::UpdateCurrentMainStats()
{

}

void Character::UpdateCalculatedStats()
{
	_stats.calculatedStats.maxActionPoints = (_stats.currentMainStats.agility / 2) + 5;
	_stats.calculatedStats.armorClass = _stats.currentMainStats.agility;
	_stats.calculatedStats.carryWeight = 25 + (_stats.currentMainStats.strength * 25);
	_stats.calculatedStats.critialChance = _stats.currentMainStats.luck;
	_stats.calculatedStats.damageResist = 0;
	_stats.calculatedStats.healingRate = std::max((int)_stats.currentMainStats.endurance / 3, 1);
	_stats.calculatedStats.maxHealth = 15 + (2 * _stats.currentMainStats.endurance) + _stats.currentMainStats.strength;
	_stats.calculatedStats.meleeDamage = std::max((int)_stats.currentMainStats.strength - 5, 1);
	_stats.calculatedStats.partyLimit = _stats.currentMainStats.charisma / 2;
	_stats.calculatedStats.poisonResistance = _stats.currentMainStats.endurance * 5;
	_stats.calculatedStats.radiationResistance = _stats.currentMainStats.endurance * 2;
	_stats.calculatedStats.squence = _stats.currentMainStats.perception * 2;
	_stats.calculatedStats.skillRate = _stats.currentMainStats.intelligence * 2 + 5;
}

void Character::UpdateSkills()
{

}

std::string Character::GetName() const
{
	return _name;
}

Stats Character::GetStats() const
{
	return _stats;
}

void Character::SetStats(const Stats& stats)
{
	_stats = stats;

	_updateStats = true;
	_updateSkills = true;
}

Skills Character::GetSkills() const
{
	return _skills;
}

void Character::SetSkills(const Skills& skills)
{
	_skills = skills;

	_updateStats = true;
	_updateSkills = true;
}

std::vector<Perk> Character::GetPerks() const
{
	return _perks;
}

void Character::SetPerks(const std::vector<Perk>& perks)
{
	_perks = perks;

	_updateStats = true;
	_updateSkills = true;
}

std::vector<Trait> Character::GetTraits() const
{
	return _traits;
}

void Character::SetTraits(const std::vector<Trait>& traits)
{
	_traits = traits;
}

std::vector<Item> Character::GetItems()
{
	std::vector<Item> items;

	for (const std::shared_ptr<Item>& itemPtr : _items)
	{
		items.push_back(*itemPtr);
	}

	return items;
}

void Character::SetItems(const std::vector<Item>& items)
{
	_items.clear();
	_items.shrink_to_fit();

	for (const Item& item : items)
	{
		_items.push_back(std::make_shared<Item>(item));
	}

	_updateStats = true;
	_updateSkills = true;
}

std::shared_ptr<Item>& Character::AddItem(const Item& item)
{
	_items.push_back(std::make_shared<Item>(item));
	return _items[_items.size() - 1];
}

Equipment Character::GetEquipment() const
{
	return _equipment;
}

void Character::SetEquipment(const Equipment& equipment)
{
	_equipment = equipment;
}

bool Character:: SelectAnimation(const std::string& animationName, const bool& loop)
{
	if (_currentAnimationName != animationName || !_currentAnimation)
	{
		auto animation = _animations.find(animationName);
	    if (animation != _animations.end())
	    {
	    	_currentAnimation = &animation->second;
	    	_animationLoop = loop;
	    	return true;
	    }

	    else
	    {
	    	LogColor("Animation: " << animationName << " on character: " << _name << " does not exist!", LOG_RED);
	    	return false;
	    }
	}

	_animationLoop = loop;
	return true;
}

void Character::Update()
{
	if (_updateStats)
	{
		UpdateCurrentMainStats();
		UpdateCalculatedStats();

		_updateStats = false;
	}

	if (_updateSkills)
	{
		UpdateSkills();

		_updateSkills = false;
	}

	_currentAnimation->Update();
}

void Character::DrawStatic(const Vector2& pos, const float& scale)
{
	DrawTextureScale(_staticSprite, pos, scale, WHITE);
}

void Character::DrawIcon(const Vector2& pos, const float& scale)
{
	DrawTextureScale(_iconSprite, pos, scale, WHITE);
}

bool Character::DrawAnimation(const Vector2& pos, const float& scale)
{
	if (!_currentAnimation->Draw(pos, 0, scale))
	{
		_currentAnimation->Start(!_animationLoop);
	}

	return _currentAnimation->Draw(pos, 0, scale);
}

Stats CalculateStats(const MainStats& mainStats)
{
	Stats stats;

	stats.mainStats = mainStats;
	stats.currentMainStats = mainStats;

	stats.calculatedStats.maxActionPoints = (stats.currentMainStats.agility / 2) + 5;
	stats.calculatedStats.armorClass = stats.currentMainStats.agility;
	stats.calculatedStats.carryWeight = 25 + (stats.currentMainStats.strength * 25);
	stats.calculatedStats.critialChance = stats.currentMainStats.luck;
	stats.calculatedStats.damageResist = 0;
	stats.calculatedStats.healingRate = std::max((int)stats.currentMainStats.endurance / 3, 1);
	stats.calculatedStats.maxHealth = 15 + (2 * stats.currentMainStats.endurance) + stats.currentMainStats.strength;
	stats.calculatedStats.meleeDamage = std::max((int)stats.currentMainStats.strength - 5, 1);
	stats.calculatedStats.partyLimit = stats.currentMainStats.charisma / 2;
	stats.calculatedStats.poisonResistance = stats.currentMainStats.endurance * 5;
	stats.calculatedStats.radiationResistance = stats.currentMainStats.endurance * 2;
	stats.calculatedStats.squence = stats.currentMainStats.perception * 2;
	stats.calculatedStats.skillRate = stats.currentMainStats.intelligence * 2 + 5;

	stats.variableStats.health = stats.calculatedStats.maxHealth;
	stats.variableStats.actionPoints = stats.calculatedStats.maxActionPoints;
	stats.variableStats.rads = 0;
	stats.variableStats.experiencePoints = 0;
	stats.variableStats.level = 1;

	return stats;
}