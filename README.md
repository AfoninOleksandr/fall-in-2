# Fall In 2

A game inspired by the Fallout universe the Darkest Dungeon gameplay flow

# To Do

- Level scene where one changes rooms and encouters
- Way to serialize level layouts and the encounters within
- Base scene to choose companions
- Way to serialize overall progress
- Combat mechanics
- Overall different world locations to explore

# Perhaps

- Multiplayer through online co-op
- Multiplayer through pvp of each others parties and perhaps base raiding