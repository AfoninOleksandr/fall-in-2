#pragma once
#include "Other/log.h"
#include "MyRaylib.h"
#include "Event.h"

#include "Character.h"

class Services;

class CombatManager : public EventListener
{
private:

	// Ptr of global services
	Services* _services;

	void Init();

	void AddSelfAsListener() override;
	void OnEvent(std::shared_ptr<const Event>& event) override;

	void ResolveCombat(Character* attacker, Character* defender);
	int ResolveUnarmed(const Character* attacker, const Character* defender) const;
	int ResolveMelee(const Character* attacker, const Character* defender) const;
	int ResolveGun(const Character* attacker, const Character* defender) const;

public:

	CombatManager(Services* servicesIn);
	~CombatManager();
};

class CombatEvent : public Event
{
public:

	Character* attacker;
	Character* defender;

	CombatEvent(Character* attackerIn, Character* defenderIn) : attacker(attackerIn), defender(defenderIn) {}
};