#pragma once
#include "Other/log.h"
#include "MyRaylib.h"

#include <map>

#include "Utility.h"

#include "Character/CharacterStats.h"

enum ItemType : unsigned int
{
	JUNK = 0,
	QUEST,
	CAPS,
	CLOTHING,
	ARMOR,
	FOOD,
	CONSUMABLE,
	MELEE,
	GUN,
	AMMO
};

enum GunType : unsigned int
{
	SMALLGUN = 0,
	BIGGUN,
	ENERGYWEAPON
};

struct ItemInfo
{
	float weight;

	unsigned int durability;
	unsigned int maxDurability;

	unsigned int damageThreshold;

	unsigned int uses;
	unsigned int maxUses;

	unsigned int ammo;
	unsigned int maxAmmo;
	GunType gunType;

	std::pair<unsigned int, unsigned int> damage;
	unsigned int actionPointCost;
};

struct Item
{
	std::string name;
	ItemType type;

	// Textures and animations
	Texture2D staticSprite;
	Texture2D iconSprite;

	std::map<std::string, Animation> animations;
	Animation* currentAnimation;
	std::string currentAnimationName;

	// Audio
	std::map<std::string, Sound> sounds;

	// Stats
	std::vector<StatSkillTypeValue> bonuses;

	// Item relevant info
	ItemInfo itemInfo;

	// TODO SPECIAL FLAGS
	// Flags
};

struct Equipment
{
	std::weak_ptr<Item> mainWeapon;
	std::weak_ptr<Item> armor;
};