#pragma once
#include "Other/log.h"
#include "MyRaylib.h"
#include "Event.h"

class Services;

class CombatEncounterUI : public EventListener
{
private:

	// Ptr of global services
	Services* _services;

	void Init();

	void AddSelfAsListener() override;
	void OnEvent(std::shared_ptr<const Event>& event) override;

public:

	CombatEncounterUI(Services* servicesIn);
	~CombatEncounterUI();

	void Update();
	void Draw();
};