#pragma once
#include "Other/log.h"
#include "MyRaylib.h"

#include <map>

#include "Utility.h"

#include "Character/CharacterStats.h"
#include "Character/CharacterPerks.h"
#include "Character/CharacterTraits.h"
#include "Item.h"

class Character
{
private:

	std::string _name;
	bool _enemy;

	// Textures and animations
	Texture2D _staticSprite;
	Texture2D _iconSprite;

	std::map<std::string, Animation> _animations;
	Animation* _currentAnimation;
	std::string _currentAnimationName;
	bool _animationLoop = false;

	// Audio
	std::map<std::string, Sound> _sounds;

	// Stats
	Stats _stats;

	bool _updateStats = true;
	bool _lvlUp = false;

	// Skills
	Skills _skills;

	bool _updateSkills = true;

	// TODO ACTUAL PERKS
	// Perks
	std::vector<Perk> _perks;

	// TODO ACTUAL TRAITS
	// Traits
	std::vector<Trait> _traits;

	// Items
	std::vector<std::shared_ptr<Item>> _items;
	float _currentWeight;

	// Equipped
	Equipment _equipment;

	// TODO SPECIAL FLAGS
	// Flags

	void UpdateCurrentMainStats();
	void UpdateCalculatedStats();
	void UpdateSkills();

public:

	Character(const std::string& nameIn, const bool& enemyIn, const Stats& statsIn, const Skills& skillsIn, const std::vector<Perk> perksIn, const std::vector<Trait> traitsIn, const Texture2D& staticSpriteIn, const Texture2D& iconSpriteIn);
	Character(const std::string& nameIn, const bool& enemyIn, const Stats& statsIn, const Skills& skillsIn, const std::vector<Perk> perksIn, const std::vector<Trait> traitsIn, const Texture2D& staticSpriteIn, const Texture2D& iconSpriteIn, const std::map<std::string, Animation>& animationsIn, const std::map<std::string, Sound>& soundsIn);
	~Character();

	// Name
	std::string GetName() const;

	// Stats
	Stats GetStats() const;
	void SetStats(const Stats& stats);

	// Skills
	Skills GetSkills() const;
	void SetSkills(const Skills& combatSkills);

	// Perks
	std::vector<Perk> GetPerks() const;
	void SetPerks(const std::vector<Perk>& perks);

	// Traits
	std::vector<Trait> GetTraits() const;
	void SetTraits(const std::vector<Trait>& traits);

	// Items
	std::vector<Item> GetItems();
	void SetItems(const std::vector<Item>& items);
	std::shared_ptr<Item>& AddItem(const Item& item);

	// Equipment
	Equipment GetEquipment() const;
	void SetEquipment(const Equipment& equipment);

	bool SelectAnimation(const std::string& animationName, const bool& loop);

	void Update();

	void DrawStatic(const Vector2& pos, const float& scale);
	void DrawIcon(const Vector2& pos, const float& scale);
	bool DrawAnimation(const Vector2& pos, const float& scale);
};