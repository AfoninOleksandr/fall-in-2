#pragma once
#include "Other/log.h"
#include "MyRaylib.h"
#include "Event.h"

#include "Character.h"

class Services;

class CombatManager;
class CombatEncounterUI;

enum Position : unsigned int
{
	L1 = 0,
	L2,
	L3,
	L4,
	R1,
	R2,
	R3,
	R4,
};

class CombatEncounter : public EventListener
{
private:

	// Ptr of global services
	Services* _services;

	// Position location
	std::vector<std::pair<Position, Vector2>> _positions;

	// Characters
	std::vector<Character*> _characters;
	std::vector<std::pair<Position, Character*>> _characterPositions;

	// UI
	std::unique_ptr<CombatEncounterUI> _UI;

	// Combat manager
	std::unique_ptr<CombatManager> _combatManager;

	void Init();

	void AddSelfAsListener() override;
	void OnEvent(std::shared_ptr<const Event>& event) override;

public:

	CombatEncounter(Services* servicesIn, const std::vector<Character*>& charactersIn);
	~CombatEncounter();

	void Update();
	void Draw();
};