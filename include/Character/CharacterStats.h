#pragma once

enum StatSkillTypeEnum : unsigned int
{
    MAINSTATS = 0,
    CALCULATEDSTATS,
    VARIABLESTATS,
    COMBATSKILLS,
    ACTIVESKILLS,
    PASSIVESKILLS
};

struct StatSkillTypeValue
{
	// These nested pairs represent the stat/skill type and the specific one and also the stat/skill value
	std::pair<std::pair<StatSkillTypeEnum, unsigned int>, unsigned int> statSkillTypeValue;
};

// https://fallout.fandom.com/wiki/Fallout_2_derived_statistics
// Various types of stats
struct MainStats
{
	unsigned int strength;
	unsigned int perception;
	unsigned int endurance;
	unsigned int charisma;
	unsigned int intelligence;
	unsigned int agility;
	unsigned int luck;
};

enum MainStatsEnum : unsigned int
{
    STRENGTH = 0,
    PERCEPTION,
    ENDURANCE,
    CHARISMA,
    INTELLIGENCE,
    AGILITY,
    LUCK
};

struct CalculatedStats
{
    unsigned int maxActionPoints;
    unsigned int armorClass;
    unsigned int carryWeight;
    unsigned int critialChance;
    unsigned int damageResist;
    unsigned int healingRate;
    unsigned int maxHealth;
    unsigned int meleeDamage;
    unsigned int partyLimit;
    unsigned int poisonResistance;
    unsigned int radiationResistance;
    unsigned int squence;
    unsigned int skillRate;
};

enum CalculatedStatsEnum : unsigned int
{
    MAXACTIONPOINTS = 0,
    ARMORCLASS,
    CARRYWEIGHT,
    CRITICALCHANCE,
    DAMAGERESISTANCE,
    HEALINGRATE,
    MAXHEALTH,
    MELEEDAMAGE,
    PARTYLIMIT,
    POISONRESISTANCE,
    RADIATIONRESISTANCE,
    SEQUENCE,
    SKILLRATE
};

struct VariableStats
{
    unsigned int health;
    unsigned int actionPoints;

    unsigned int rads;

    unsigned int experiencePoints;
    unsigned int level;
};

enum VariableStatsEnum : unsigned int
{
    HEALTH = 0,
    ACTIONPOINTS,
    RADIATION,
    EXPERIENCEPOINTS,
    LEVEL
};

struct Stats
{
    MainStats mainStats;
    MainStats currentMainStats;
    CalculatedStats calculatedStats;
    VariableStats variableStats;
};

Stats CalculateStats(const MainStats& mainStats);

// https://fallout.fandom.com/wiki/Fallout_2_skills
// Various skills
struct CombatSkills
{
    unsigned int smallGuns;
    unsigned int bigGuns;
    unsigned int energyWeapons;
    unsigned int unarmed;
    unsigned int meleeWeapons;
    unsigned int throwing;
};

enum CombatSkillsEnum : unsigned int
{
    SMALLGUNS = 0,
    BIGGUNS,
    ENERGYWEAPONS,
    UNARMED,
    MELEEWEAPONS,
    THROWING
};

struct ActiveSkills
{
    unsigned int firstAid;
    unsigned int doctor;
    unsigned int sneak;
    unsigned int lockPick;
    unsigned int steal;
    unsigned int traps;
    unsigned int science;
    unsigned int repair;
};

enum ActiveSkillsEnum : unsigned int
{
    FIRSTAID = 0,
    DOCTOR,
    SNEAK,
    LOCKPICK,
    STEAL,
    TRAPS,
    SCIENCE,
    REPAIR
};

struct PassiveSkills
{
    unsigned int speech;
    unsigned int barter;
    unsigned int gambling;
    unsigned int outdoorsman;
};

enum PassiveSkillsEnum : unsigned int
{
    SPEECH = 0,
    BARTER,
    GAMBLING,
    OUTDOORSMAN
};

struct Skills
{
    CombatSkills combatSkills;
    ActiveSkills activeSkills;
    PassiveSkills passiveSkills;
};