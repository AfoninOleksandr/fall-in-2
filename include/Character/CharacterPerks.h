#pragma once

#include "CharacterStats.h"

// https://fallout.fandom.com/wiki/Fallout_2_perks
// Perks
struct Perk
{
	std::string name;
	
	unsigned int requiredLvl;
	std::vector<StatSkillTypeValue> otherRequirements;
	unsigned int ranks;

	std::vector<StatSkillTypeValue> _bonuses;
};