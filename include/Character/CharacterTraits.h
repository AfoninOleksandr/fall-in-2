#pragma once

// https://fallout.fandom.com/wiki/Fallout_2_traits
// Traits
struct Trait
{
	std::string name;
};