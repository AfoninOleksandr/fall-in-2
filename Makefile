# Variables
CXX = g++
SRC_DIR = src
OBJ_DIR = src
BIN_DIR = bin
DATA_DIR = data
INCLUDE_DIR = include
LIB_DIR = lib

# Source and Object files
SRCS = $(wildcard $(SRC_DIR)/*.cpp)
OBJS = $(SRCS:$(SRC_DIR)/%.cpp=$(OBJ_DIR)/%.o)

# Common compiler flags
COMMON_FLAGS = -std=c++17 -fmax-errors=5 -Werror -Wno-narrowing -Wno-enum-compare --static -I$(INCLUDE_DIR) -L$(LIB_DIR)

# Debug and Release flags
DEBUG_FLAGS = -O0
RELEASE_FLAGS = -O2 -s -mwindows

# Libraries to link against
LIBS =$(LIB_DIR)/libraylib.a -lopengl32 -lgdi32 -lwinmm

# Rules
all: clean debug

debug: $(OBJS)
	$(CXX) $(OBJS) $(COMMON_FLAGS) $(DEBUG_FLAGS) -o $(BIN_DIR)/main $(LIBS)

release: $(OBJS)
	$(CXX) $(OBJS) $(COMMON_FLAGS) $(RELEASE_FLAGS) -o $(BIN_DIR)/main $(LIBS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CXX) $(COMMON_FLAGS) $(DEBUG_FLAGS) -c $< -o $@

# Emscripten compiler
EMXX = em++
EMSCRIPTEN_FLAGS = -w -fms-extensions -std=c++17 -D_DEFAULT_SOURCE -Wno-missing-braces -Wunused-result -Os -I$(INCLUDE_DIR) -DPLATFORM_WEB --preload-file $(DATA_DIR) --shell-file $(LIB_DIR)/shell.html -s USE_GLFW=3 -s ASYNCIFY -s TOTAL_MEMORY=67108864 -s FORCE_FILESYSTEM=1 -s EXPORTED_FUNCTIONS=['_free','_malloc','_main'] -s EXPORTED_RUNTIME_METHODS=ccall -L$(LIB_DIR) -lwebraylib

web: $(SRCS)
	$(EMXX) $(SRCS) $(EMSCRIPTEN_FLAGS) -o $(BIN_DIR)/main.html
	$(MAKE) clean_objs_wsl

clean:
	rm -f $(OBJ_DIR)/*.o

clean_objs_wsl:
	del /F /Q "$(OBJ_DIR)\*.o"
	del /F /Q "$(SRC_DIR)\*.o